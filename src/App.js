import React, { Component } from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/jquery/dist/jquery.min.js';
import '../node_modules/popper.js/dist/umd/popper.min.js';
import '../node_modules/bootstrap/dist/js/bootstrap.min.js';
import axios from 'axios';
//import logo from './logo.svg';
import './App.css';

class App extends Component {

  constructor()
  {
    super();
    this.state = {
      search1: '',
      facet:[],
      results: [],
      total_result: 0
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onClicked = this.onClicked.bind(this);
  }

  onSubmit(e) {
    e.preventDefault();
    console.log(this.state.search1);
    
    axios.post('/',{query: this.state.search1}).then(res=>{

      console.log(res.data);
      this.setState({ 
        results: res.data.results,
        facet: res.data.facet,
        total_result: res.data.total_result
       });

    }).catch(err =>{

        Promise.reject(new Error('Failed Fetching Data!!'));
    });

    
  }
  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
    //console.log(e.target.value);
  }
  onClicked(e)
  {
      console.log(e.target.value);
      if(this.state.search1=="")
      {
        axios.post('/filtered1',{parent_category: e.target.value}).then(res=>{

          console.log(res.data);
          this.setState({ 
            results: res.data.results,
            total_result: res.data.total_result
           });
    
        }).catch(err =>{
    
            Promise.reject(new Error('Failed Fetching Data!!'));
        });
    
      }
      else
      {
        axios.post('/filtered2',{query: this.state.search1, parent_category: e.target.value}).then(res=>{

          console.log(res.data);
          this.setState({ 
            results: res.data.results,
            total_result: res.data.total_result
           });
    
        }).catch(err =>{
    
            Promise.reject(new Error('Failed Fetching Data!!'));
        });
      }
  }
  render() {
    const res = (


      <div className="table-responsive-sm">
                    <table className="table table-hover">
                     <thead className="text-muted">
                      <tr>
                       <th scope="col"></th>
                       <th scope="col">Product Name</th>
                       <th scope="col" width="120">Price</th>
                      </tr>
                     </thead>
                     <tbody>
                      {this.state.results.map(prod=>{
                       return (<tr key={prod.id}>
                       <td>
                        <figure className="media">
                         <div className="img-wrap"><img src={ prod.image_url } className="img-thumbnail img-sm"/></div>
                         </figure>
                         </td>
                         <td>
                         <figcaption className="media-body">
                          <h6 className="title">{ prod.product_name }</h6>
                         </figcaption>
                         
                        </td>

	                   
                       <td>
                        <div className="price-wrap">
                         <var className="price">{ prod.price }</var>
                        </div> 
	                   </td>
                       
                    </tr>);
                    })}
                    
                   </tbody>
                  </table>
                 </div>
    );
    const facets = (

      <div className="btn-group">
        { 
          this.state.facet.map((cat, index)=>{
              //let rand=Math.random().toString(36).substring(2);
              let rand = Object.keys(cat).join('');
              //console.log(rand);
              return (<input type="button" key={index} className="btn btn-primary" name={rand} onClick={this.onClicked} value={ rand }/>);
          })
      
        }
      </div>

    );
  return (
    <div className="App">
        <form className="d-flex justify-content-center" onSubmit={this.onSubmit}>
            <div className="col-auto">
              <input type="text" className="form-control mb-2" id="search1" placeholder="Search Box" name="search1" value={this.state.search1}
              onChange={this.onChange}/>
            </div>
            <div className="col-auto">
              <button type="submit" className="btn btn-primary mb-2">Submit</button>
            </div>
        </form>
        <br/>
        <h2>Total Result: { this.state.total_result }</h2>
        <br/>
        { this.state.facet.length>0? facets:'' }
        <br/> <br/>
        {this.state.total_result>0? res:''}

    </div>
  );
  }
}


export default App;
